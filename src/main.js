/**
 * Задание 12 - Создать интерфейс StarWars DB для данных из SWAPI.
 *
 * Используя SWAPI, вывести информацию по всех планетам с пагинацией и возможностью просмотреть доп.
 * информацию в модальном окне с дозагрузкой смежных ресурсов из каждой сущности.
 *
 * Данные для отображения в карточке планеты:
 * 1. Наименование (name)
 * 2. Диаметр (diameter)
 * 3. Население (population)
 * 4. Уровень гравитации (gravity)
 * 5. Природные зоны (terrain)
 * 6. Климатические зоны (climate)
 *
 * При клике по карточке отображаем в модальном окне всю информацию
 * из карточки, а также дополнительную:
 * 1. Список фильмов (films)
 * - Номер эпизода (episode_id)
 * - Название (title)
 * - Дата выхода (release_date)
 * 2. Список персонажей
 * - Имя (name)
 * - Пол (gender)
 * - День рождения (birth_year)
 * - Наименование родного мира (homeworld -> name)
 *
 * Доп. требования к интерфейсу:
 * 1. Выводим 10 карточек на 1 странице
 * 2. Пагинация позволяет переключаться между страницами, выводить общее количество страниц и текущие выбранные
 * элементы в формате 1-10/60 для 1 страницы или 11-20/60 для второй и т.д.
 * 3. Используем Bootstrap для создания интерфейсов.
 * 4. Добавить кнопку "Показать все" - по клику загрузит все страницы с планетами и выведет
 * информацию о них в един (опцианально)
 */

(async () => {
     function getPlanetsData(pageNumber = 1){
        return fetch(`https://swapi.dev/api/planets/?page=${pageNumber}`)
            .then(response => response.json())
            .then(({ count: total, results: planets }) => ({ total, planets }));
    }

    function renderPlanetCards(planets) {
        const cardsHtml = planets.map(
            planet =>
                `<div class="card">
                    <div class="card-info">
                        <h5 class="card-title">${planet.name}</h5>
                        <ul class="list">
                            ${
                                ['diameter', 'population', 'gravity', 'terrain', 'climate'].map(
                                    key => `<li>${key}: ${planet[key]}</li>`
                                ).join('')
                            }
                        </ul>
                    </div>
                    <button type="button" class="btn btn-primary card-button-js" data-url="${planet.url}" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        More
                    </button>
                </div>`
        ).join('');
        document.querySelector('.cards').innerHTML = cardsHtml;
    }

    function renderPagination(total, pageSize = 10) {
        let pages = '<li class="page-item active"><a class="page-link" data-page="1" href="#">1</a></li>';
        for (let i = 1; i < Math.round(total / pageSize); i++) {
            pages += `<li class="page-item"><a class="page-link" data-page="${i + 1}" href="#">${i + 1}</a></li>`;
        }

        let paginationHtml =
            `<ul class="pagination">
                <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
                ${pages}
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
            </ul>`;
        document.querySelector('.pagination-js').innerHTML = paginationHtml;
    }

    function managePagination(total, pageSize = 10){
        document.querySelector('.pagination-js').addEventListener(
            'click',
            async event => {
                event.preventDefault();
                if(event.target.hasAttribute('data-page')){
                    const pageNumber = event.target.getAttribute('data-page');
                    const planetsData = await getPlanetsData(pageNumber);
                    renderPlanetCards(planetsData.planets);
                    document.querySelectorAll('.page-item').forEach(
                        item => item.classList.remove('active')
                    )
                    event.target.parentNode.classList.add('active')
                }
                if(event.target.parentNode.textContent === 'Previous'){
                    const activePage = document.querySelector('.active');
                    const activePageNumber = activePage.firstChild.getAttribute('data-page');
                    const planetsData = await getPlanetsData(activePageNumber-1);
                    renderPlanetCards(planetsData.planets);
                    activePage.classList.remove('active');
                    activePage.previousElementSibling.classList.add('active');
                }
                if(event.target.parentNode.textContent === 'Next'){
                    const activePage = document.querySelector('.active');
                    const activePageNumber = activePage.firstChild.getAttribute('data-page');
                    const planetsData = await getPlanetsData(+activePageNumber+1);
                    renderPlanetCards(planetsData.planets);
                    activePage.classList.remove('active');
                    activePage.nextElementSibling.classList.add('active');
                }
                document.querySelectorAll('.page-item').forEach(
                    item => item.classList.remove('disabled')
                )
                const activePage = document.querySelector('.active');
                if(activePage.firstChild.getAttribute('data-page') === '1'){
                    activePage.previousElementSibling.classList.add('disabled')
                }
                if(activePage === document.querySelector('.pagination').lastElementChild.previousElementSibling){
                    activePage.nextElementSibling.classList.add('disabled')
                }
            }
        )
    }

    function getPlanet(url){
        return fetch(`${url}`)
            .then(response => response.json())
    }

    function manageModal(){
         document.querySelector('.cards').addEventListener(
             'click',
             async event => {
                 if(event.target.classList.contains('card-button-js')){
                     const url = event.target.getAttribute('data-url');
                     const planet = await getPlanet(url);
                     document.querySelector('.modal-title').innerHTML = planet.name
                     const films = await Promise.all(
                         planet.films.map(
                             film => fetch(film).then(response => response.json())
                         )
                     )
                     const residents = await Promise.all(
                         planet.residents.map(
                             resident => fetch(resident).then(response => response.json())
                         )
                     )
                     let filmsList = films.map(
                         film => `<ul class="list">
                                     <li>Episode: ${film.episode_id}</li>
                                     <li>Title: ${film.title}</li>
                                     <li>Release date: ${film.release_date}</li>
                                 </ul>`
                     ).join('');
                     if(filmsList === ''){
                         filmsList = 'no films'
                     }
                     let residentsList = (await Promise.all(residents.map(
                         async resident => `<ul class="list">
                                                <li>Name: ${resident.name}</li>
                                                <li>Gender: ${resident.gender}</li>
                                                <li>Bitth year: ${resident.birth_year}</li>
                                                <li>Homeworld: ${(await fetch(resident.homeworld).then(response => response.json())).name}</li>
                                            </ul>`
                     ))).join('');
                     if(residentsList === ''){
                         residentsList = 'no residents'
                     }
                     const modalHtml = `<h6>Films</h6>
                                        ${filmsList}
                                        <h6>Residents</h6>
                                        ${residentsList}`
                     document.querySelector('.modal-body').innerHTML = modalHtml;
                 }
             }
         )
    }

    function showAllPlanets(total){
         document.querySelector('.button-js').addEventListener(
             'click',
            async (event) => {
                 if(event.target.textContent === 'Показать все'){
                     const allPlanets = [];
                     for(let i = 1; i <= total; i++){
                         allPlanets.push(await getPlanet(`https://swapi.dev/api/planets/${i}`));
                     }
                     renderPlanetCards(allPlanets);
                     document.querySelector('.pagination-js').innerHTML = '';
                     event.target.textContent = 'Скрыть все'
                 }
                 else if(event.target.textContent === 'Скрыть все'){
                     const planetsData = await getPlanetsData();
                     renderPlanetCards(planetsData.planets);
                     renderPagination(planetsData.total);
                     event.target.textContent = 'Показать все'
                 }
            }
        )
    }
    const planetsData = await getPlanetsData();
    renderPlanetCards(planetsData.planets);
    renderPagination(planetsData.total);
    managePagination(planetsData.total);
    showAllPlanets(planetsData.total);
    manageModal()
})();